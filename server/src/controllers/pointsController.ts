import { Request, Response } from "express";
import knex from "../database/connection";

class PointController {
  async index(request: Request, response: Response) {
    const { city, uf, items } = request.query;

    const parsedItems = String(items)
      .split(",")
      .map((item) => Number(item.trim()));

    const filterdPoints = await knex("points")
      .join("points_items", "points_items.point_id", "=", "points.id")
      .whereIn("points_items.item_id", parsedItems)
      .andWhere("points.city", String(city))
      .andWhere("points.uf", String(uf))
      .distinct()
      .select("points.*");

    const serializedPoints = filterdPoints.map((point) => {
      return {
        ...point,
        image_url: `http://192.168.1.13:3333/uploads/${point.image}`,
      };
    });

    return response.json(serializedPoints);
  }
  async show(request: Request, response: Response) {
    const { id } = request.params;

    const point = await knex("points").where("id", id).first();

    if (!point)
      return response.status(400).json({ message: "Point not found." });

    const items = await knex("items")
      .join("points_items", "points_items.item_id", "=", "items.id")
      .where("points_items.point_id", id)
      .select("items.title");

    const serializedPoints = {
      ...point,
      image_url: `http://192.168.1.13:3333/uploads/${point.image}`,
    };

    return response.json({ serializedPoints, items });
  }

  async create(request: Request, response: Response) {
    const {
      name,
      email,
      whatsapp,
      latitude,
      longitude,
      city,
      uf,
      items,
    } = request.body;

    const point = {
      image: request.file.filename,
      name,
      email,
      whatsapp,
      latitude,
      longitude,
      city,
      uf,
    };

    const trx = await knex.transaction();

    const insertedIds = await trx("points")
      .insert(point)
      .then((id) => {
        return id;
      })
      .catch((error) => {
        console.log(
          `Error inserting into table 'points'. Error Message: ${error}`
        );
        return [-1];
      });

    const point_id = insertedIds[0];

    const pointItems = items
      .split(",")
      .map((item: string) => Number(item.trim()))
      .map((item_id: number) => {
        return {
          item_id,
          point_id,
        };
      });

    await trx("points_items")
      .insert(pointItems)
      .then((id) => {
        trx.commit();
        return id;
      })
      .catch((error) => {
        console.log(
          `Error inserting into table 'points_items'. Error Message: ${error}`
        );
        trx.rollback();
        return [-1];
      });

    return response.json({
      id: point_id,
      ...point,
    });
  }
}

export default PointController;
